/*
 * Copyright Damir Holovati (C) 2017
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef DYNAMIC_ARRAY_H
#define DYNAMIC_ARRAY_H

#ifdef __cplusplus
extern "C" {
#endif

#define GROWTH_FACTOR 2

#define DYNAMIC_ARRAY_COMBINE1(X,Y) X##Y  // helper macro
#define DYNAMIC_ARRAY_COMBINE(X,Y) DYNAMIC_ARRAY_COMBINE1(X,Y)

#define TYPENAME(N) DYNAMIC_ARRAY_COMBINE(N, _t)
#define TYPEFUNC(N,SUFF) DYNAMIC_ARRAY_COMBINE(N,DYNAMIC_ARRAY_COMBINE(_,SUFF))

#define DYNAMIC_ARRAY_DECLARE(NAME, T)                                  \
  typedef struct {                                                      \
    T* data;                                                            \
    size_t size;                                                        \
    size_t capacity;                                                    \
  } TYPENAME(NAME);                                                     \
                                                                        \
  void TYPEFUNC(NAME,init)(TYPENAME(NAME)* ctx);                        \
                                                                        \
  void TYPEFUNC(NAME,destroy)(TYPENAME(NAME)* ctx);                     \
                                                                        \
  T* TYPEFUNC(NAME,at)(TYPENAME(NAME)* ctx, size_t idx);                \
                                                                        \
  T* TYPEFUNC(NAME,back)(TYPENAME(NAME)* ctx);                          \
                                                                        \
  size_t TYPEFUNC(NAME,capacity)(TYPENAME(NAME)* ctx);                  \
                                                                        \
  void TYPEFUNC(NAME,clear)(TYPENAME(NAME)* ctx);                       \
                                                                        \
  T* TYPEFUNC(NAME,data)(TYPENAME(NAME)* ctx);                          \
                                                                        \
  uint8_t TYPEFUNC(NAME,empty)(TYPENAME(NAME)* ctx);                    \
                                                                        \
  T* TYPEFUNC(NAME,front)(TYPENAME(NAME)* ctx);                         \
                                                                        \
  void TYPEFUNC(NAME,pop_back)(TYPENAME(NAME)* ctx);                    \
                                                                        \
  void TYPEFUNC(NAME,reserve)(TYPENAME(NAME)* ctx, size_t req_size);    \
                                                                        \
  void TYPEFUNC(NAME,push_back_ptr)(TYPENAME(NAME)* ctx, T* pval);      \
                                                                        \
  void TYPEFUNC(NAME,push_back)(TYPENAME(NAME)* ctx, T v);              \
                                                                        \
  void TYPEFUNC(NAME,resize)(TYPENAME(NAME)* ctx, size_t sz, T* v);     \
                                                                        \
  void TYPEFUNC(NAME,shrink_to_fit)(TYPENAME(NAME)* ctx);               \
                                                                        \
  size_t TYPEFUNC(NAME,size)(TYPENAME(NAME)* ctx)

#define DYNAMIC_ARRAY_DEFINE(NAME, T, MEM_ALLOC, MEM_FREE)              \
                                                                        \
    void TYPEFUNC(NAME,init)(TYPENAME(NAME)* ctx) {                     \
      memset(ctx, 0, sizeof(TYPENAME(NAME)));                           \
    }                                                                   \
                                                                        \
    void TYPEFUNC(NAME,destroy)(TYPENAME(NAME)* ctx) {                  \
      if (ctx->data != NULL && ctx->capacity > 0)                       \
        (MEM_FREE)(ctx->data);                                          \
    }                                                                   \
                                                                        \
    /* Fix this, what if idx > size or data ptr = NULL? */              \
    T* TYPEFUNC(NAME,at)(TYPENAME(NAME)* ctx, size_t idx) {             \
      return &ctx->data[idx];                                           \
    }                                                                   \
                                                                        \
    /* Same problem as above */                                         \
    T* TYPEFUNC(NAME,back)(TYPENAME(NAME)* ctx) {                       \
      return &ctx->data[ctx->size - 1];                                 \
    }                                                                   \
                                                                        \
    size_t TYPEFUNC(NAME,capacity)(TYPENAME(NAME)* ctx) {               \
      return ctx->capacity;                                             \
    }                                                                   \
                                                                        \
    void TYPEFUNC(NAME,clear)(TYPENAME(NAME)* ctx) {                    \
      ctx->size = 0;                                                    \
    }                                                                   \
                                                                        \
    T* TYPEFUNC(NAME,data)(TYPENAME(NAME)* ctx) {                       \
      return ctx->data;                                                 \
    }                                                                   \
                                                                        \
    uint8_t TYPEFUNC(NAME,empty)(TYPENAME(NAME)* ctx) {                 \
      return ctx->size ? 0 : 1;                                         \
    }                                                                   \
                                                                        \
    T* TYPEFUNC(NAME,front)(TYPENAME(NAME)* ctx) {                      \
      return ctx->data;                                                 \
    }                                                                   \
                                                                        \
    void TYPEFUNC(NAME,pop_back)(TYPENAME(NAME)* ctx) {                 \
      if (ctx->size > 0)                                                \
        --(ctx->size);                                                  \
    }                                                                   \
                                                                        \
    void TYPEFUNC(NAME,reserve)(TYPENAME(NAME)* ctx, size_t req_size) { \
      T* new_data;                                                      \
                                                                        \
      if (req_size <= ctx->size)                                        \
        return;                                                         \
                                                                        \
      new_data = MEM_ALLOC(sizeof(T) * req_size);                       \
                                                                        \
      if (ctx->size > 0) {                                              \
        memcpy(new_data, ctx->data, sizeof(T) * ctx->size);             \
        MEM_FREE(ctx->data);                                            \
      }                                                                 \
                                                                        \
      ctx->data = new_data;                                             \
      ctx->capacity = req_size;                                         \
    }                                                                   \
                                                                        \
    void TYPEFUNC(NAME,push_back_ptr)(TYPENAME(NAME)* ctx, T* pval) {   \
      size_t new_cap;                                                   \
                                                                        \
      new_cap = ctx->capacity * GROWTH_FACTOR;                          \
                                                                        \
      if (new_cap == 0)                                                 \
        TYPEFUNC(NAME,reserve)(ctx, 1);                                 \
      else if(ctx->capacity == ctx->size)                               \
        TYPEFUNC(NAME,reserve)(ctx, new_cap);                           \
                                                                        \
      *TYPEFUNC(NAME,at)(ctx, ctx->size) = *pval;                       \
                                                                        \
      ++(ctx->size);                                                    \
    }                                                                   \
                                                                        \
    void TYPEFUNC(NAME,push_back)(TYPENAME(NAME)* ctx, T v) {           \
      TYPEFUNC(NAME,push_back_ptr)(ctx, &v);                            \
    }                                                                   \
                                                                        \
    void TYPEFUNC(NAME,resize)(TYPENAME(NAME)* ctx, size_t sz, T* v) {  \
      if (sz <= ctx->size) {                                            \
        ctx->size = sz;                                                 \
        return;                                                         \
      }                                                                 \
                                                                        \
      TYPEFUNC(NAME,reserve)(ctx, sz);                                  \
                                                                        \
      ctx->size = sz;                                                   \
                                                                        \
      if (v == NULL) {                                                  \
        memset(ctx->data, 0, sizeof(T) * sz);                           \
        return;                                                         \
      }                                                                 \
                                                                        \
      size_t cur_idx;                                                   \
      T *elem;                                                          \
      for(cur_idx = ctx->size - 1; cur_idx < sz; ++cur_idx) {           \
        elem = TYPEFUNC(NAME,at)(ctx, cur_idx);                         \
        *elem = *v;                                                     \
      }                                                                 \
    }                                                                   \
                                                                        \
    void TYPEFUNC(NAME,shrink_to_fit)(TYPENAME(NAME)* ctx) {            \
      size_t cur_cap = ctx->capacity;                                   \
      size_t cur_size = ctx->size;                                      \
      T* new_data;                                                      \
                                                                        \
      if(cur_size == 0) {                                               \
        MEM_FREE(ctx->data);                                            \
        ctx->capacity = 0;                                              \
        return;                                                         \
      }                                                                 \
                                                                        \
      if (cur_cap == 0 || (cur_cap % cur_size) != 0)                    \
        return;                                                         \
                                                                        \
      new_data = MEM_ALLOC(sizeof(T) * cur_size);                       \
                                                                        \
      memcpy(new_data, ctx->data, sizeof(T) * cur_size);                \
                                                                        \
      MEM_FREE(ctx->data);                                              \
                                                                        \
      ctx->data = new_data;                                             \
      ctx->capacity = cur_size;                                         \
    }                                                                   \
                                                                        \
    size_t TYPEFUNC(NAME,size)(TYPENAME(NAME)* ctx) {                   \
      return ctx->size;                                                 \
    }
    
#ifdef __cplusplus
}
#endif

#endif /* DYNAMIC_ARRAY_H */
